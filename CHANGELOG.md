# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.5.1] - 2020-11-02
### Added
- Add telecom company assignation to Contract when a new contract is created with the Change holder wizard. [!30](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/30)

## [0.5.0] - 2020-10-28
### Fixed
- Fix contract creation in Change holder wizard. Remove telecom company assignation [!27](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/27)

### Added
- Set change_address boolean to True in the new order created with Change Address wizard [!25](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/25)
- Add coverage availability to Change Address wizard [!20](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/20) and [!26](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/26)
- Add notes field to Change Address wizard [!28](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/28)

### Changed
- Change the field internet_previous_provider by provider to refer the service providers in a Change Address. Also refactor the Change address wizard view. [!29](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/29)

## [0.4.0] - 2020-04-28
### Fixed
- Use a unicode strings in the activity subject [!21](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/21)

### Added
- Track errors and deploys in Bugsnag [!22](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/-/merge_requests/22)

## [v0.3.0] - 2020-03-24
### Added
- Create activities to register the wizard execution [!17](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/17)
- Add start/end reason to contract and set this field with the Change Holder wizard[!19](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/19)

## [v0.2.1] - 2020-01-10

### Fixed
- Change contract holder in the past [!10](https://gitlab.com/coopdevs/contract_changes_wizards_somconnexio/merge_requests/10)

## [v0.2.0] - 2019-11-11
