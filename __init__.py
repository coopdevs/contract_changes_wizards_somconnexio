from trytond.pool import Pool

from models.wizards.change_contract_holder.change_contract_holder import ChangeContractHolder
from models.wizards.change_contract_holder.start import ChangeContractHolderStart
from models.wizards.change_contract_holder.check_data import ChangeContractHolderCheckData

from models.wizards.change_contract_email.change_contract_email import ChangeContractEmail
from models.wizards.change_contract_email.start import ChangeContractEmailStart
from models.wizards.change_contract_email.select_contracts import ChangeContractEmailSelectContracts

from models.wizards.change_contract_bank_account.change_contract_bank_account import ChangeContractBankAccount
from models.wizards.change_contract_bank_account.start import ChangeContractBankAccountStart
from models.wizards.change_contract_bank_account.select_contracts import ChangeContractBankAccountSelectContracts

from models.wizards.change_contract_address.change_contract_address import ChangeContractAddress
from models.wizards.change_contract_address.start import ChangeContractAddressStart

import os
import bugsnag
from version import VERSION


api_key = os.getenv('BUGSNAG_CONTRACT_CHANGES_WIZARDS_SOMCONNEXIO_API_KEY')
bugsnag.configure(api_key=api_key, app_version=VERSION)


def register():
    Pool.register(
        ChangeContractHolder,
        ChangeContractEmail,
        ChangeContractBankAccount,
        ChangeContractAddress,
        module='contract_changes_wizards_somconnexio', type_='wizard')

    Pool.register(
        ChangeContractHolderStart,
        ChangeContractHolderCheckData,
        ChangeContractEmailStart,
        ChangeContractEmailSelectContracts,
        ChangeContractBankAccountStart,
        ChangeContractBankAccountSelectContracts,
        ChangeContractAddressStart,
        module='contract_changes_wizards_somconnexio', type_='model')
