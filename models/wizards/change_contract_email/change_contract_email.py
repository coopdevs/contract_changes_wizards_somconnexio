from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition
from trytond.wizard import Button

from ...services.change_contracts_email import ChangeContractsEmailService
from ...activities.activity_from_email_contract_change import ActivityFromEmailContractChange


class ChangeContractEmail(Wizard):
    """ Change Contract Email """

    __name__ = 'change.contract.email'

    start = StateView(
        'change.contract.email.start',
        'contract_changes_wizards_somconnexio.change_contract_email_start',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'select_contracts', 'tryton-ok', default=True)
        ])
    select_contracts = StateView(
        'change.contract.email.select_contracts',
        'contract_changes_wizards_somconnexio.change_contract_email_select_contracts',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'change_contract_email', 'tryton-ok', default=True)
        ])
    change_contract_email = StateTransition()

    def default_select_contracts(self, fields):
        contracts = Pool().get('contract').search([
            ('party', '=', self.start.party.id),
            ('state', '=', 'confirmed')
        ])
        return {
            'party': self.start.party.id,
            'email': self.start.email.id,
            'contracts': [contract.id for contract in contracts]
        }

    def transition_change_contract_email(self):
        ChangeContractsEmailService(
            self.start.email.id,
            self.select_contracts.contracts
        ).run()

        self._create_activities()

        return 'end'

    def _create_activities(self):
        for contract in self.select_contracts.contracts:
            ActivityFromEmailContractChange(
                contract,
                self.start.description,
                self.start.email.rec_name
            ).create()
