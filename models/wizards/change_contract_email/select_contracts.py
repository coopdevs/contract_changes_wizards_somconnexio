from trytond.model import ModelView, fields


class ChangeContractEmailSelectContracts(ModelView):
    """ Change Contract Email select contracts """

    __name__ = 'change.contract.email.select_contracts'

    party = fields.Many2One('party.party', 'Party', readonly=True)
    email = fields.Many2One('party.contact_mechanism', 'Contact email', readonly=True)
    contracts = fields.One2Many('contract', None, 'Contracts to update')
