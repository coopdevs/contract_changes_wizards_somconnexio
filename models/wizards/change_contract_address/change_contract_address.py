import logging

from trytond.wizard import Wizard, StateAction, StateView, StateTransition
from trytond.wizard import Button

from ...services.change_contract_address import ChangeContractAddressService


logger = logging.getLogger(__name__)


class ChangeContractAddress(Wizard):
    """ Change Contract Address """

    __name__ = 'change.contract.address'

    start = StateView(
        'change.contract.address.start',
        'contract_changes_wizards_somconnexio.change_contract_address_start',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'change_contract_address', 'tryton-ok', default=True)
        ]
    )
    view_contract = StateAction('eticom.action_contract_view')
    change_contract_address = StateTransition()

    def transition_change_contract_address(self):
        new_contract = ChangeContractAddressService(self.start).run()
        # Save the contract to search in the StateAction
        self.new_contract = new_contract
        return 'view_contract'

    def do_view_contract(self, action):
        data = {
            'res_id': self.new_contract.id,
        }
        return action, data
