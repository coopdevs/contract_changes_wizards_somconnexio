from trytond.model import ModelView, fields


class ChangeContractHolderStart(ModelView):
    """ Change Contract Holder start """

    __name__ = 'change.contract.holder.start'

    contract = fields.Many2One(
        'contract',
        'Contract',
        required=True,
        domain=[('state', '=', 'confirmed')])
    party = fields.Many2One('party.party', 'Party', required=True)
    end_date = fields.Date('End Date', required=True)
    description = fields.Char("Description", required=True)
