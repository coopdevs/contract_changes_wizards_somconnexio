from trytond.model import ModelView, fields
from trytond.pyson import Eval


class ChangeContractHolderCheckData(ModelView):
    """ Change Contract Holder check data """

    __name__ = 'change.contract.holder.check_data'

    end_date = fields.Date(
        'End Date',
        required=True,
        readonly=True)
    contract = fields.Many2One(
        'contract',
        'Selected Contract',
        readonly=True)
    party = fields.Many2One(
        'party.party',
        'Party',
        required=True,
        readonly=True)
    description = fields.Char(
        "Description",
        required=True,
        readonly=True)
    delivery_mobile = fields.Many2One(
        'party.address',
        'Delivery Mobile',
        domain=[('party', '=', Eval('party'))],
        depends=['party'])
    delivery_internet = fields.Many2One(
        'party.address',
        'Delivery Internet',
        domain=[('party', '=', Eval('party'))],
        depends=['party'])
    internet_address = fields.Many2One(
        'party.address',
        'Internet Address',
        domain=[('party', '=', Eval('party'))],
        depends=['party'])
    contact_address = fields.Many2One(
        'party.address',
        'Contact Address',
        domain=[('party', '=', Eval('party'))],
        required=True,
        depends=['party'])
    contact_email = fields.Many2One(
        'party.contact_mechanism',
        'Contact email',
        depends=['party'],
        required=True,
        domain=[
            ('type', '=', 'email'),
            ('party', '=', Eval('party'))])
    receivable_bank_account = fields.Many2One(
        'bank.account',
        'Receivable Bank Account',
        domain=[
            ('owners', '=', Eval('party'))],
        depends=['party'],
        required=True,
        help='Party bank account')
