from trytond.model import ModelView, fields
from trytond.pyson import Eval


class ChangeContractBankAccountStart(ModelView):
    """ Change Contract BankAccount start """

    __name__ = 'change.contract.bank_account.start'

    party = fields.Many2One('party.party', 'Party', required=True)
    bank_account = fields.Many2One(
        'bank.account',
        'Bank Account',
        required=True,
        depends=['party'],
        domain=[('owners', '=', Eval('party'))])
    description = fields.Char("Description", required=True)
