from trytond.model import ModelView, fields


class ChangeContractBankAccountSelectContracts(ModelView):
    """ Change Contract BankAccount select contracts """

    __name__ = 'change.contract.bank_account.select_contracts'

    party = fields.Many2One('party.party', 'Party', readonly=True)
    bank_account = fields.Many2One('bank.account', 'Bank Account', readonly=True)
    contracts = fields.One2Many('contract', None, 'Contracts to update')
