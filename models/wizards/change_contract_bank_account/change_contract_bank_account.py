from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition
from trytond.wizard import Button

from ...services.change_contract_bank_account import ChangeContractsBankAccountService
from ...activities.activity_from_bank_account_contract_change import ActivityFromBankAccountContractChange


class ChangeContractBankAccount(Wizard):
    """ Change Contract Bank Account """

    __name__ = 'change.contract.bank_account'

    start = StateView(
        'change.contract.bank_account.start',
        'contract_changes_wizards_somconnexio.change_contract_bank_account_start',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'select_contracts', 'tryton-ok', default=True)
        ]
    )
    select_contracts = StateView(
        'change.contract.bank_account.select_contracts',
        'contract_changes_wizards_somconnexio.change_contract_bank_account_select_contracts',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'change_receivable_bank_account', 'tryton-ok', default=True)
        ]
    )
    change_receivable_bank_account = StateTransition()

    def default_select_contracts(self, fields):
        contracts = Pool().get('contract').search([
            ('party', '=', self.start.party.id),
            ('state', '=', 'confirmed')
        ])
        return {
            'party': self.start.party.id,
            'bank_account': self.start.bank_account.id,
            'contracts': [contract.id for contract in contracts]
        }

    def transition_change_receivable_bank_account(self):
        ChangeContractsBankAccountService(
            self.start.bank_account.id,
            self.select_contracts.contracts
        ).run()

        self._create_activities()

        return 'end'

    def _create_activities(self):
        for contract in self.select_contracts.contracts:
            ActivityFromBankAccountContractChange(
                contract,
                self.start.description,
                self.start.bank_account.rec_name
            ).create()
