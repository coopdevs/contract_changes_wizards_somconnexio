from trytond.pool import Pool


class ChangeContractsBankAccountService:
    """
    Give a list of contracts and a bank account, changes the bank accounts related
    to the contracts in batch.
    """

    def __init__(self, bank_account, contracts):
        self.bank_account = bank_account
        self.contracts = contracts

    def run(self):
        for contract in self.contracts:
            contract.receivable_bank_account = self.bank_account

        Pool().get('contract').write(
            [contract for contract in self.contracts],
            {'receivable_bank_account': self.bank_account}
        )
