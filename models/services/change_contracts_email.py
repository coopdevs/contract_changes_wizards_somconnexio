from trytond.pool import Pool


class ChangeContractsEmailService:
    """
    Gives a list of contracts and an email, changes the email related
    to the contracts in batch.
    """

    def __init__(self, email, contracts):
        self.email = email
        self.contracts = contracts

    def run(self):
        for contract in self.contracts:
            contract.contact_email = self.email

        Pool().get('contract').write(
            [contract for contract in self.contracts],
            {'contact_email': self.email}
        )
