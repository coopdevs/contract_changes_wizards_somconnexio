from trytond.pool import Pool


class CopyLinesService:
    """
    Given a collection of lines and a start date, it creates a copy of active lines in the collection.
    Also filters only the kit parent lines.

    Returns the new collection of lines
    """

    def __init__(self, lines, start_date):
        self.lines = lines
        self.start_date = start_date

    def run(self):
        new_lines = []

        for line in self.lines:
            if not line.current() or line.kit_parent_line:
                continue

            new_lines.append(self._create_new_line(line))

        return new_lines

    def _create_new_line(self, line):
        new_line = Pool().get('contract.line')()
        new_line.start_date = self.start_date
        new_line.service = line.service
        new_line.description = line.description
        new_line.unit_price = line.unit_price
        return new_line
