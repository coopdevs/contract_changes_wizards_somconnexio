from datetime import timedelta
from dateutil import relativedelta

from trytond.pool import Pool

from .copy_lines_service import CopyLinesService


class ChangeContractHolderService:
    """
    Gives a contracts, a date and a party, ends the contract in the date
    received and create a new contract with the received party as holder.

    Returns the new contract created.
    """

    def __init__(self, change_data):
        self.contract = change_data.contract
        self.party = change_data.party
        self.end_date = change_data.end_date

        self.receivable_bank_account = change_data.receivable_bank_account
        self.contact_email = change_data.contact_email
        self.contact_address = change_data.contact_address
        self.internet_address = change_data.internet_address
        self.delivery_internet = change_data.delivery_internet
        self.delivery_mobile = change_data.delivery_mobile

    def run(self):
        # Create new contract with the new holder
        new_contract = self._create_new_contract()
        self._finish_old_contract()

        return new_contract

    def _finish_old_contract(self):
        self.contract.ending_reason = 'holder change'
        self.contract.save()

        self.contract.finish_at_date(self.end_date)

    def _create_new_contract(self):
        """
        Create new contract with the same data of the contract selected in start wizard step and assign the new holder.
        """
        contract = self.contract
        self.new_contract = Pool().get('contract')()
        self.start_period_date = self.end_date + timedelta(days=1)
        self._set_values_from_change_data()
        self._copy_values_from_contract()
        self.new_contract.lines = CopyLinesService(contract.lines, self.start_period_date).run()
        self.new_contract.starting_reason = 'holder change'
        self.new_contract.save()
        Pool().get('contract').confirm([self.new_contract])

        return self.new_contract

    def _set_values_from_change_data(self):
        self.new_contract.party = self.party.id
        self.new_contract.receivable_bank_account = self.receivable_bank_account.id
        self.new_contract.contact_email = self.contact_email.id
        self.new_contract.contact_address = self.contact_address.id
        self.new_contract.start_period_date = self.start_period_date
        self.new_contract.first_invoice_date = (self.end_date + relativedelta.relativedelta(months=1)).replace(day=1)
        if self.internet_address:
            self.new_contract.internet_address = self.internet_address.id
        if self.delivery_internet:
            self.new_contract.delivery_internet = self.delivery_internet.id
        if self.delivery_mobile:
            self.new_contract.delivery_mobile = self.delivery_mobile.id

    def _copy_values_from_contract(self):
        self.new_contract.service_type = self.contract.service_type
        self.new_contract.freq = self.contract.freq
        self.new_contract.interval = self.contract.interval
        self.new_contract.line = self.contract.line
        self.new_contract.active_service = self.contract.active_service
        self.new_contract.data_activated = self.contract.data_activated
        self.new_contract.msidsn = self.contract.msidsn
        self.new_contract.telecom_company = self.contract.telecom_company
