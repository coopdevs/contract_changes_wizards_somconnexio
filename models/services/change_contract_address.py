from trytond.pool import Pool


class ChangeContractAddressService:
    """
    Given a party and the data related to the service, creates a new contract with the new address of the party.

    Returns the new contract created.
    """

    def __init__(self, data):
        self.data = data

    def run(self):
        new_contract = Pool().get('eticom.contract')()

        new_contract.party = self.data.party.id
        new_contract.service = self.data.service
        new_contract.internet_now = self.data.internet_now
        new_contract.internet_contract = self.data.internet_contract
        new_contract.internet_phone = self.data.internet_phone
        new_contract.internet_phone_minutes = self.data.internet_phone_minutes
        new_contract.internet_speed = self.data.internet_speed
        new_contract.internet_city = self.data.internet_city
        new_contract.internet_street = self.data.internet_street
        new_contract.internet_zip = self.data.internet_zip
        new_contract.internet_country = self.data.internet_country
        new_contract.internet_subdivision = self.data.internet_subdivision
        new_contract.provider_change_address = self.data.provider

        new_contract.bank_iban_service = self.data.receivable_bank_account.rec_name
        new_contract.bank_owner_service_name = self.data.party.rec_name
        new_contract.notes = "Canvi d'ubicacio {} {}: {}".format(
            self.data.internet_now,
            self.data.internet_contract,
            self.data.notes
        )
        coverage = self._create_coverage_availability()
        new_contract.coverage_availability = coverage.id

        new_contract.change_address = True

        new_contract.save()

        return new_contract

    def _create_coverage_availability(self):
        coverage = Pool().get('coverage.availability')()
        coverage.adsl = self.data.adsl_coverage
        coverage.mm_fiber = self.data.mm_fiber_coverage
        coverage.vdf_fiber = self.data.vdf_fiber_coverage
        coverage.save()
        return coverage
