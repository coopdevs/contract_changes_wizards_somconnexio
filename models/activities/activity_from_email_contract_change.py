from .activity_from_contract_change import ActivityFromContractChange


class ActivityFromEmailContractChange(ActivityFromContractChange):
    ACTIVITY_TYPE_NAME = 'Canvi de dades de contacte'
    SUBJECT_MESSAGE = u'Email change: {}'
