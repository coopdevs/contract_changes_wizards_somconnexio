# -*- coding: utf-8 -*-
from .activity_from_contract_change import ActivityFromContractChange


class ActivityFromBankAccountContractChange(ActivityFromContractChange):
    ACTIVITY_TYPE_NAME = 'Modificació Compte Bancari'
    SUBJECT_MESSAGE = u'IBAN change: {}'
