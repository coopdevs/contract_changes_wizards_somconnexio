from datetime import date

from trytond.pool import Pool
from trytond.transaction import Transaction


class ActivityFromContractChange():
    def __init__(self, contract, description, subject):
        self.contract = contract
        self.subject = self.SUBJECT_MESSAGE.format(subject)
        self.description = description

    def create(self):
        activity = Pool().get('activity.activity')()
        activity.employee = Transaction().context.get('employee')
        activity.resource = ('contract', self.contract)
        activity.party = self.contract.party
        activity.subject = self.subject
        activity.description = self.description
        activity.start_date = date.today()
        activity.state = 'held'

        activity_type = Pool().get('activity.type').search(
            [('name', '=', self.ACTIVITY_TYPE_NAME)],
            limit=1
        )[0]
        activity.activity_type = activity_type.id

        activity.save()

        return activity
