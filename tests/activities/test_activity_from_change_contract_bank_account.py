# -*- coding: utf-8 -*-
from faker import Faker
from mock import Mock, patch
import unittest2 as unittest

from models.activities.activity_from_bank_account_contract_change import ActivityFromBankAccountContractChange


class ActivityFromBankAccountContractChangeTestCase(unittest.TestCase):

    @patch('models.activities.activity_from_contract_change.Transaction')
    @patch('models.activities.activity_from_contract_change.Pool', return_value=Mock(spec=['get']))
    def test_create(self, MockPool, MockTransaction):
        bank_account = Faker().iban()
        contract = Mock(spec=['party'])

        activity_type = Mock(spec=['id'])
        MockActivity = Mock(return_value=Mock(spec=['save']))
        MockActivityType = Mock(spec=['search'])

        def activity_sype_search_side_effect(domain, limit):
            if domain == [('name', '=', 'Modificació Compte Bancari')] and limit == 1:
                return [activity_type]

        MockActivityType.search.side_effect = activity_sype_search_side_effect

        def pool_get_side_effect(model):
            if model == 'activity.type':
                return MockActivityType
            if model == 'activity.activity':
                return MockActivity

        MockPool.return_value.get.side_effect = pool_get_side_effect

        activity = ActivityFromBankAccountContractChange(
            contract,
            "Some Description",
            bank_account
        ).create()

        activity.save.assert_called_once()

        self.assertEqual(activity.resource, ('contract', contract))
        self.assertEqual(activity.party, contract.party)
        self.assertEqual(activity.subject, 'IBAN change: {}'.format(bank_account))
        self.assertIsInstance(activity.subject, unicode)
        self.assertEqual(activity.description, "Some Description")
        self.assertEqual(activity.activity_type, activity_type.id)
        self.assertEqual(activity.state, 'held')
