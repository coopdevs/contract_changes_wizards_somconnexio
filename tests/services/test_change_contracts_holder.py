from datetime import timedelta
from faker import Faker
from mock import Mock, patch
import unittest2 as unittest

from models.services.change_contract_holder import ChangeContractHolderService


class FakeChangeData:
    def __init__(self):

        self.contract = Mock(spec=[
           'finish_at_date',
           'service_type',
           'freq',
           'interval',
           'line',
           'active_service',
           'data_activated',
           'msidsn',
           'telecom_company',
           'lines',
           'ending_reason',
           'save'
        ])
        self.contract.lines = []
        self.party = Mock(spec=['id'])
        self.end_date = Faker().date_time().date()

        self.receivable_bank_account = Mock(specr=['id'])
        self.contact_email = Mock(specr=['id'])
        self.contact_address = Mock(specr=['id'])
        self.internet_address = Mock(specr=['id'])
        self.delivery_internet = Mock(specr=['id'])
        self.delivery_mobile = Mock(specr=['id'])


class ChangeContractHolderServiceTestCase(unittest.TestCase):

    @patch('models.services.change_contract_holder.CopyLinesService', return_value=Mock(spec=['run']))
    @patch('models.services.change_contract_holder.Pool', return_value=Mock(spec=['get']))
    def test_run(self, MockPool, MockCopyLineService):
        change_data = FakeChangeData()

        expected_contract = Mock(spec=['save'])

        MockContract = Mock(
            spec=['confirm'],
            return_value=expected_contract
        )

        def pool_get_side_effect(model):
            if model == 'contract':
                return MockContract
        MockPool.return_value.get.side_effect = pool_get_side_effect

        contract = ChangeContractHolderService(change_data).run()

        MockCopyLineService.return_value.run.assert_called_once()
        MockCopyLineService.assert_called_once_with(
            change_data.contract.lines, change_data.end_date + timedelta(days=1))

        self.assertEqual(expected_contract.starting_reason, 'holder change')
        expected_contract.save.assert_called_once()

        self.assertEqual(change_data.contract.ending_reason, 'holder change')
        change_data.contract.save.assert_called_once()
        change_data.contract.finish_at_date.assert_called_once_with(change_data.end_date)

        self.assertEqual(contract, expected_contract)
