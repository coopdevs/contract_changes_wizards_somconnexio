from faker import Faker
from mock import Mock, patch
import unittest2 as unittest

from models.services.copy_lines_service import CopyLinesService


class CopyLinesServiceTestCase(unittest.TestCase):

    @patch('models.services.copy_lines_service.Pool')
    def test_run_not_current_lines(self, MockPool):
        start_date = Faker().date_time().date()

        old_line = Mock(spec=['current'])
        old_line.current.return_value = False
        lines = [old_line]

        expected_lines = []

        lines = CopyLinesService(lines, start_date).run()

        self.assertEqual(lines, expected_lines)

    @patch('models.services.copy_lines_service.Pool')
    def test_run_child_line(self, MockPool):
        start_date = Faker().date_time().date()

        child_line = Mock(spec=['current', 'kit_parent_line'])
        child_line.current.return_value = True
        child_line.kit_parent_line = object()
        lines = [child_line]

        expected_lines = []

        lines = CopyLinesService(lines, start_date).run()

        self.assertEqual(lines, expected_lines)

    @patch('models.services.copy_lines_service.Pool', return_value=Mock(spec=['get']))
    def test_run_parent_line(self, MockPool):
        start_date = Faker().date_time().date()

        parent_line = Mock(spec=[
            'current',
            'kit_parent_line',
            'service',
            'description',
            'unit_price'
        ])
        parent_line.current.return_value = True
        parent_line.kit_parent_line = None
        lines = [parent_line]

        expected_line = Mock()
        expected_lines = [expected_line]

        MockContractLine = Mock(
            return_value=expected_line
        )

        def pool_get_side_effect(model):
            if model == 'contract.line':
                return MockContractLine
        MockPool.return_value.get.side_effect = pool_get_side_effect

        lines = CopyLinesService(lines, start_date).run()

        self.assertEqual(lines, expected_lines)

        first_line = lines[0]
        self.assertEqual(first_line.start_date, start_date)
        self.assertEqual(first_line.service, parent_line.service)
        self.assertEqual(first_line.description, parent_line.description)
        self.assertEqual(first_line.unit_price, parent_line.unit_price)
