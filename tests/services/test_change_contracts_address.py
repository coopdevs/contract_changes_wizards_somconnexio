from mock import Mock, patch
import unittest2 as unittest

from models.services.change_contract_address import ChangeContractAddressService


class FakeData:
    def __init__(self):
        self.party = Mock(spec=['id', 'rec_name'])
        self.service = "adsl"
        self.internet_now = "adsl"
        self.internet_contract = "adsl"
        self.internet_phone = "phone"
        self.internet_phone_minutes = "100"
        self.internet_speed = "120MB"
        self.internet_city = "Barcelona"
        self.internet_street = "Street"
        self.internet_zip = "08001"
        self.internet_country = 123
        self.internet_subdivision = 123
        self.provider = None
        self.receivable_bank_account = Mock(spec=['id', 'rec_name'])
        self.notes = "Notes"

        self.adsl_coverage = "NoCoverage"
        self.mm_fiber_coverage = "NoCoverage"
        self.vdf_fiber_coverage = "NoCoverage"


class ChangeContractAddressServiceTestCase(unittest.TestCase):

    @patch('models.services.change_contract_address.Pool', return_value=Mock(spec=['get']))
    def test_run(self, MockPool):
        data = FakeData()

        expected_order = Mock(spec=['save'])
        expected_coverage = Mock(spec=['save', 'id'])

        MockOrder = Mock(
            return_value=expected_order
        )
        MockCoverageAvailavility = Mock(
            return_value=expected_coverage
        )

        def pool_get_side_effect(model):
            if model == 'eticom.contract':
                return MockOrder
            if model == 'coverage.availability':
                return MockCoverageAvailavility
        MockPool.return_value.get.side_effect = pool_get_side_effect

        new_order = ChangeContractAddressService(data).run()

        self.assertEqual(new_order, expected_order)
        new_order.save.assert_called_once()

        expected_coverage.save.assert_called_once()
        self.assertEqual(expected_coverage.adsl, data.adsl_coverage)
        self.assertEqual(expected_coverage.mm_fiber, data.mm_fiber_coverage)
        self.assertEqual(expected_coverage.vdf_fiber, data.vdf_fiber_coverage)

        self.assertEqual(new_order.party, data.party.id)
        self.assertEqual(new_order.service, data.service)
        self.assertEqual(new_order.internet_now, data.internet_now)
        self.assertEqual(new_order.internet_contract, data.internet_contract)
        self.assertEqual(new_order.internet_phone, data.internet_phone)
        self.assertEqual(new_order.internet_phone_minutes, data.internet_phone_minutes)
        self.assertEqual(new_order.internet_speed, data.internet_speed)
        self.assertEqual(new_order.internet_city, data.internet_city)
        self.assertEqual(new_order.internet_street, data.internet_street)
        self.assertEqual(new_order.internet_zip, data.internet_zip)
        self.assertEqual(new_order.internet_country, data.internet_country)
        self.assertEqual(new_order.internet_subdivision, data.internet_subdivision)
        self.assertEqual(new_order.provider_change_address, data.provider)
        self.assertEqual(new_order.bank_iban_service, data.receivable_bank_account.rec_name)
        self.assertEqual(new_order.bank_owner_service_name, data.party.rec_name)
        self.assertEqual(
            new_order.notes,
            "Canvi d'ubicacio {} {}: {}".format(data.internet_now, data.internet_contract, data.notes)
        )
        self.assertTrue(new_order.change_address)
